# Assistance for player for game - The Werewolves of Millers Hollow
The Werewolves of Millers Hollow

User can upload history call via web portalr.
User can input current situation with known roles in particular postions, then based on historical data set, give suggestions on the roles for unknown positions.

## Prerequisites
Node.js v.8.12.0

## Setup

- Clone the repo and cd into the directory:

```
      $ git clone https://github.com/seaskyv/wolf.git
      cd wolf
```
- Install dependencies using npm:

```
      $ npm install
```
- Start the service:
  - Normal start :
  ```
      $ npm run start
  
  ```
  - Run in dev ENV with nodemon :

  ```  
      $ npm run devstart
  ```
- Open the app in your browser by default at `http://localhost:3300/`.
- Config file : `config.json`
- backend server hostname and port number can be specified by ENV variable backendhost and backendport

## Contribute
Under the MIT License. 

#### Basic Git Workflow

- Clone the repo and cd into the directory:

```
      $ git clone https://github.com/seaskyv/AU-Lottery-generator.git
```

- Crfeate a branch for your fixes or new features:

```
      $ git checkout -b branch_name_here
```

- Make your update.

- Push to your fork Open a Pull Request!

## Related repository
```https://github.com/seaskyv/AU-Lottery-generator_API.git```
